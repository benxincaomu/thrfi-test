/**
 * 
 */
package com.sft.thriftMutiDemo.client;

import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TMultiplexedProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransportException;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.ZooKeeper;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sft.service.HelloWorldService;

/**
 * @author sft
 *
 *         2016年5月17日
 */
public class MoreMutiClient {
	private static final Logger log = Logger.getLogger(MoreMutiClient.class);
	private static Random ran = new Random();

	private static int getIndex(int size) {
		log.info("size:" + size);
		return ran.nextInt(size);
	}

	public static void main(String[] args) throws IOException, KeeperException, InterruptedException, TException {
		long a = System.currentTimeMillis();
		MyWatch watcher = new MyWatch();
		ZooKeeper zk = new ZooKeeper("192.168.2.92:2008", 120000, watcher);
		String pathName = "/thriftMuti";// 父节点名称
		List<String> serviceNameList = zk.getChildren(pathName, false);
		long b = System.currentTimeMillis();
		log.info("从zookeeper获取服务信息的时间：" + (b - a));
		// 获取服务信息
		String serviceName = serviceNameList.get(MoreMutiClient.getIndex(serviceNameList.size()));
		log.info("要使用的serviceName:" + serviceName);
		byte[] data = zk.getData(pathName + "/" + serviceName, false, null);
		zk.close();
		log.info("=======zk任务完成=======");
		JSONObject serverJSON = JSON.parseObject(new String(data));
		
		final String serverIp = serverJSON.getString("ip");
		final int serverPort = serverJSON.getInteger("port");
		// 3、阻塞调用===========================
		// 准备调用参数
		ExecutorService theadPool = Executors.newFixedThreadPool(100);
		Thread thread = new Thread() {
			/* (non-Javadoc)
			 * @see java.lang.Thread#run()
			 */
			@Override
			public void run() {
				try {
					TSocket transport = new TSocket(serverIp, serverPort);
					transport.open();
					TProtocol protocol = new TBinaryProtocol(transport);
					TMultiplexedProtocol tmp=new TMultiplexedProtocol(protocol, "hello");
					HelloWorldService.Client thriftClient = new com.sft.service.HelloWorldService.Client(tmp);
					log.info("return:" + thriftClient.sayHello("aaa"));
					transport.close();
				} catch (TTransportException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (TException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};
		for (int i = 0; i < 100; i++) {
			theadPool.execute(thread);
		}

		theadPool.shutdown();
		
		
		log.info("调用thrift用时:" + (System.currentTimeMillis() - b));
	}

}

