package com.sft.thriftAsyncDemo.server;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;
import org.apache.thrift.TMultiplexedProcessor;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.server.ServerContext;
import org.apache.thrift.server.THsHaServer;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TServerEventHandler;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TNonblockingServerSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.ZooDefs.Ids;
import org.apache.zookeeper.data.Stat;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sft.service.Hello1Impl;
import com.sft.service.HelloImpl;
import com.sft.service.HelloWorldService;
import com.sft.service.HelloWorldService1;

/**
 * @author sft
 *
 *         2016年5月17日
 */
public class AsyncServer {
	private static final Logger log = Logger.getLogger(AsyncServer.class);
	private boolean isStart = Boolean.FALSE;
	private int port = 1009;

	public static void main(String[] args) {
		AsyncServer server = new AsyncServer();
		server.startServer();
		// 一直等待，apache thrift启动完成
		synchronized (server) {
			try {
				while (!server.isStart) {
					server.wait();
				}
			} catch (InterruptedException e) {
				log.error(e);
				System.exit(-1);
			}
		}
		try {
			server.connectZk();
		} catch (IOException | KeeperException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	void connectZk() throws IOException, KeeperException, InterruptedException {
		log.info("===========开始连接zookeeper============");
		MyWatch watcher = new MyWatch();
		ZooKeeper zk = new ZooKeeper("192.168.2.92:2008", 120000, watcher);
		// 创建服务的父节点
		String pathName = "/thriftMuti";
		Stat path = null;
		try {
			path = zk.exists(pathName, watcher);
			if (path == null) {
				zk.create(pathName, "".getBytes(), Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
			}

		} catch (KeeperException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.info("=========连接zookeeper异常，程序退出===========");
			System.exit(-1);
		}
		// 添加服务
		String serviceName = "/testThriftMuti";// 调用的服务名称
		JSONObject nodeJson = new JSONObject();
		nodeJson.put("ip", "192.168.2.92");
		nodeJson.put("port", port);
		JSONArray interfaceArr = new JSONArray();
		JSONObject interfaceJson = new JSONObject();
		interfaceJson.put("name", HelloImpl.class.getName());
		JSONArray methArr = new JSONArray();
		for (Method method : HelloImpl.class.getDeclaredMethods()) {
			methArr.add(method.getName());
		}
		interfaceJson.put("methods", methArr);
		interfaceArr.add(interfaceJson);
		nodeJson.put("interfaces", interfaceArr);
		zk.create(pathName + serviceName, nodeJson.toString().getBytes(), Ids.OPEN_ACL_UNSAFE, CreateMode.EPHEMERAL_SEQUENTIAL);
	}

	void startServer() {
		new Thread() {
			@Override
			public void run() {

				// 非阻塞服务端
				try {
					log.info("TNonblockingServer start ....");
					// TProcessor tprocessor = new
					// HelloWorldService.Processor<HelloWorldService.Iface>(new
					// HelloImpl());
					TMultiplexedProcessor processor = new TMultiplexedProcessor();
					processor.registerProcessor("hello", new HelloWorldService.Processor<HelloWorldService.Iface>(new HelloImpl()));
					processor.registerProcessor("hello1", new HelloWorldService1.Processor<HelloWorldService1.Iface>(new Hello1Impl()));
					TNonblockingServerSocket tnbSocketTransport = new TNonblockingServerSocket(port);
					THsHaServer.Args tnbArgs = new THsHaServer.Args(tnbSocketTransport);
					tnbArgs.processor(processor);
					tnbArgs.transportFactory(new TFramedTransport.Factory());
					tnbArgs.protocolFactory(new TCompactProtocol.Factory());
					tnbArgs.executorService(Executors.newCachedThreadPool());

					// 使用非阻塞式IO，服务端和客户端需要指定TFramedTransport数据传输的方式
					TServer server = new THsHaServer(tnbArgs);
					server.setServerEventHandler(new StartServerEventHandler());
					server.serve();

				} catch (Exception e) {
					log.info("Server start error!!!");
					e.printStackTrace();
				}

			}
		}.start();
	}

	class StartServerEventHandler implements TServerEventHandler {
		@Override
		public void preServe() {
			/*
			 * 需要实现这个方法，以便在服务启动成功后， 通知mainProcessor： “Apache Thrift已经成功启动了”
			 */
			AsyncServer.this.isStart = true;
			synchronized (AsyncServer.this) {
				AsyncServer.this.notify();
			}
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.apache.thrift.server.TServerEventHandler#createContext(org.apache
		 * .thrift.protocol.TProtocol, org.apache.thrift.protocol.TProtocol)
		 */
		@Override
		public ServerContext createContext(TProtocol input, TProtocol output) {
			/*
			 * 无需实现
			 */
			return null;
		}

		@Override
		public void deleteContext(ServerContext serverContext, TProtocol input, TProtocol output) {
			/*
			 * 无需实现
			 */
		}

		@Override
		public void processContext(ServerContext serverContext, TTransport inputTransport, TTransport outputTransport) {
			/*
			 * 无需实现
			 */
		}
	}

	private class MyWatch implements Watcher {

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.apache.zookeeper.Watcher#process(org.apache.zookeeper.
		 * WatchedEvent)
		 */
		@Override
		public void process(WatchedEvent event) {
			// TODO Auto-generated method stub

		}

	}

}
