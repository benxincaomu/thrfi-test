/**
 * 
 */
package com.sft.thriftDemo;

import com.alibaba.fastjson.JSON;
import com.sft.service.HelloImpl;

/**
 * @author sft
 *
 * 2016年5月18日
 */
public class Test {
	public static void main(String[] args)  {
		System.out.println(JSON.toJSONString(HelloImpl.class));
	}
}
