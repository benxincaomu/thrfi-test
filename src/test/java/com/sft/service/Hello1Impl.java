/**
 * 
 */
package com.sft.service;

import org.apache.log4j.Logger;
import org.apache.thrift.TException;

import com.sft.service.HelloWorldService1.Iface;

/**
 * @author sft
 *
 *         2016年5月19日
 */
public class Hello1Impl implements Iface {
	private static final Logger log = Logger.getLogger(Hello1Impl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sft.thriftMutiDemo.service.HelloWorldService1.Iface#sayHello(java.
	 * lang.String)
	 */
	@Override
	public String sayHello(String username) throws TException {
		try {
			log.info("========暂停1秒=========");
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "1 say :" + username;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sft.thriftMutiDemo.service.HelloWorldService1.Iface#getInt()
	 */
	@Override
	public int getInt() throws TException {
		// TODO Auto-generated method stub
		return 1;
	}

}
