namespace java com.sft.thrift.iface
 
service  HelloWorldService {
  string sayHello(1:string username)
  i32 getInt()
}
