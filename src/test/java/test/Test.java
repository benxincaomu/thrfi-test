package test;
/**
 * @author sft
 *
 *         2016年5月23日
 */
public class Test {
	@org.junit.Test
	public void testReflect() throws ClassNotFoundException {
		Class<?> testClass=Class.forName("com.sft.service.HelloImpl");
		Class<?> parentClass=testClass.getInterfaces()[0];
		Class<?> outerClass=parentClass.getDeclaringClass();
		Class<?>[] inners=outerClass.getDeclaredClasses();
		System.out.println("输入的类："+testClass);
		System.out.println("是否实现了Iface接口："+parentClass.getSimpleName().equals("Iface"));
		System.out.println("所属的外部类："+outerClass);
		System.out.println("所有的内部类：");
		for(Class<?> innerClass:inners){
			System.out.println(innerClass);
		}
		
		
	}
}
