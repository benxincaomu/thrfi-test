package main;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author sft
 *
 *         2016年5月23日
 */
public class TestMain {
	@SuppressWarnings({  "resource" })
	public static void main(String[] args) {
		new ClassPathXmlApplicationContext("application.xml");
	}
}
