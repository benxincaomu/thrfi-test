package com.sft.thriftAsyncDemo.client;

import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.protocol.TMultiplexedProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.ZooKeeper;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sft.service.HelloWorldService;

/**
 * @author sft
 *
 *         2016年5月17日
 */
public class MoreAsyncClient {
	private static final Logger log = Logger.getLogger(MoreAsyncClient.class);
	private static Random ran = new Random();

	private static int getIndex(int size) {
		return ran.nextInt(size);
	}

	public static void main(String[] args) throws IOException, KeeperException, InterruptedException, TException {
		long a = System.currentTimeMillis();
		MyWatch watcher = new MyWatch();
		ZooKeeper zk = new ZooKeeper("192.168.2.92:2008", 120000, watcher);
		String pathName = "/thriftMuti";// 父节点名称
		List<String> serviceNameList = zk.getChildren(pathName, false);
		long b = System.currentTimeMillis();
		log.info("从zookeeper获取服务信息的时间：" + (b - a));
		// 获取服务信息
		String serviceName = serviceNameList.get(MoreAsyncClient.getIndex(serviceNameList.size()));
		log.info("要使用的serviceName:" + serviceName);
		byte[] data = zk.getData(pathName + "/" + serviceName, false, null);
		zk.close();
		log.info("=======zk任务完成=======");
		JSONObject serverJSON = JSON.parseObject(new String(data));

		final String serverIp = serverJSON.getString("ip");
		final int serverPort = serverJSON.getInteger("port");
		ExecutorService theadPool = Executors.newFixedThreadPool(100);
		Thread thread = new Thread() {
			Integer threadCount = 0;

			/*
			 * (non-Javadoc)
			 * 
			 * @see java.lang.Thread#run()
			 */
			@Override
			public void run() {
				long start=System.currentTimeMillis();
				// 非阻塞客户端调用
				TTransport transport = null;
				try {
					transport = new TFramedTransport(new TSocket(serverIp, serverPort));
					// 协议要和服务端一致
					TProtocol protocol = new TCompactProtocol(transport);
					TMultiplexedProtocol tmp = new TMultiplexedProtocol(protocol, "hello");
					HelloWorldService.Client client = new HelloWorldService.Client(tmp);
					transport.open();
					client.sayHello(threadCount++ + "");
					log.info("本次调用时间:" + (System.currentTimeMillis()-start));
//					log.info("return :"+result);
					transport.close();

				} catch (TTransportException e) {
					e.printStackTrace();
				} catch (TException e) {
					e.printStackTrace();
				} finally {
					if (null != transport) {
						transport.close();
					}
				}
			}
		};
		for (int i = 0; i < 100; i++) {
			theadPool.execute(thread);
		}

		theadPool.shutdown();

		log.info("调用thrift用时:" + (System.currentTimeMillis() - b));
	}

}
