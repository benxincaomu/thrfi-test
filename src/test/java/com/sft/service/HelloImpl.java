/**
 * 
 */
package com.sft.service;

import org.apache.thrift.TException;

import com.sft.service.HelloWorldService.Iface;

/**
 * @author sft
 *
 * 2016年5月19日
 */
public class HelloImpl implements Iface {
//	private static final Logger log = Logger.getLogger(HelloImpl.class);
	/* (non-Javadoc)
	 * @see com.sft.thriftMutiDemo.service.HelloWorldService.Iface#sayHello(java.lang.String)
	 */
	@Override
	public String sayHello(String username) throws TException {
		return "0 say:"+username;
	}

	/* (non-Javadoc)
	 * @see com.sft.thriftMutiDemo.service.HelloWorldService.Iface#getInt()
	 */
	@Override
	public int getInt() throws TException {
		
		return 0;
	}

}
