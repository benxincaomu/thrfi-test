package com.thrift.zookeeper.server;

/**
 * @author sft
 *
 *         2016年5月23日
 */
@SuppressWarnings("serial")
public class ThriftExcption extends Exception {
	public ThriftExcption() {

	}
	public ThriftExcption(String message) {
		super(message);
	}
	public ThriftExcption(String message, Throwable cause) {
        super(message, cause);
    }
	public ThriftExcption(Throwable cause) {
        super(cause);
    }
}
